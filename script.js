let visible = true;   

let pass1 = document.querySelector('#password1');
let pass2 = document.querySelector('#password2');
let pass3 = document.querySelector('#password3');

let showPassword = () => {
    document.getElementById('show1').addEventListener('click', () => showPass(pass1));
    document.getElementById('show2').addEventListener('click', () => showPass(pass2));
    document.getElementById('show3').addEventListener('click', () => showPass(pass3));
    }

let showPass = pass => {
    if(visible) {
        pass.type = 'text';
        visible = false;
    }
    else  {
        pass.type= 'password'
        visible = true;
    }
}

let changePassword = () => {
    document.getElementById('change').addEventListener('click', () => {
        if(pass2.value == pass3.value){
            alert(`Now the password is ${pass2.value}!`)
            pass2.value = '';
            pass3.value = '';
            pass1.value = '';
        } 
        else {
            alert(`The password doesen't match!`);
            pass2.value = '';
            pass3.value = '';
            pass1.value = '';
        }
    })
}

changePassword();
showPassword();